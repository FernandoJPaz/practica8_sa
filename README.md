# SA-Practica8
Practica 8 de Software Avanzado

## Descripcion
* Crear al menos dos contenedores en Docker Compose que involucren:
* * Un servidor web
* * Una base de datos 
* El servidor web debe hacer alguna consulta a la base (lenguaje libre) y presentar información en el puerto 80 del host.
* El Docker Compose debe poder darle los datos a la base en algún archivo que la base importe al iniciar y luego sea lo que presente.
* Entrega en repositorio y video de demostración (2 minutos máximo).

## Pre-Requisitos 
* docker
* docker-compose

## Ejecucion 
Construccion de los contenedores
```
docker-compose up -d
```
**Donde:**
* -d indica que se corre en modo demonio

**O**
```
docker-compose up -d --build 
```
**Donde:**
* -d indica que se corre en modo demonio
* --build indica que vuelva a reconstruir los contenedores